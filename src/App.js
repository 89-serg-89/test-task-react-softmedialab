import React from 'react';
import {connect} from "react-redux";
import Form from "./form";
import { getFormValues } from 'redux-form'

const App = ({ formValues }) => {
    const getEmployeeMoney = () => {
        return formValues.isPersonalIncomeTax ? formValues.amount : +formValues.amount - getPercent()
    }

    const getPercent = () => {
        return formValues.amount * .13
    }

    const getFullMoney = () => {
        return formValues.isPersonalIncomeTax ? +formValues.amount + getPercent() : formValues.amount
    }

    const filterIntl = (value) => {
        return new Intl.NumberFormat('ru-RU').format(value)
    }

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-5">
                    <Form/>
                    {
                        formValues && formValues.type_payment === 'monthly_salary'
                            ?
                            <div className="info-wrap p-4">
                                <p><span className="strong">{ filterIntl(getEmployeeMoney()) } &#8381;</span> Сотрудник будет получать на руки</p>
                                <p><span className="strong">{ filterIntl(getPercent()) } &#8381;</span> ДФЛ, 13% от оклада</p>
                                <p><span className="strong">{ filterIntl(getFullMoney()) } &#8381;</span> за сотрудника в месяц</p>
                            </div>
                            : <></>
                    }
                </div>
            </div>
        </div>
  )
}

export default connect(
    state => ({
        formValues: getFormValues('payment')(state)
    })
)(App);

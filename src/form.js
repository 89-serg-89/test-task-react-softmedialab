import React, { useState } from "react";
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'

let Form = props => {
    const { typePayment } = props
    const [showInfo, setShowInfo] = useState(false)

    const infoHandler = (e) => {
        e.preventDefault()
        setShowInfo(!showInfo)
    }

    return (
        <form className="py-4">
            <p className="title">Сумма</p>
            <div className="px-3">
                <div className="form-check">
                    <Field
                        className="form-check-input"
                        name="type_payment"
                        component="input"
                        type="radio"
                        value="monthly_salary"
                        id="monthly_salary"
                    />
                    <label htmlFor="monthly_salary">Оклад за месяц</label>
                </div>
                <div className="form-check">
                    <Field
                        className="form-check-input"
                        name="type_payment"
                        component="input"
                        type="radio"
                        value="minimum_wage"
                        id="minimum_wage"
                    />
                    <label htmlFor="minimum_wage" className="d-flex align-items-center">
                        <span>МРОТ</span>
                        <span className="info" onClick={infoHandler}>
                            {
                                !showInfo
                                    ? <i className="fas fa-info"></i>
                                    : <i className="fas fa-times"></i>
                            }
                            <span className={`tooltip ${showInfo ? 'active' : ''}`}>
                                <p>МРОТ - минимальный размер оплаты труда. Разный для разных регионов</p>
                            </span>
                        </span>
                    </label>
                </div>
                <div className="form-check">
                    <Field
                        className="form-check-input"
                        name="type_payment"
                        component="input"
                        type="radio"
                        value="payment_per_day"
                        id="payment_per_day"
                    />
                    <label htmlFor="payment_per_day">Оплата за день</label>
                </div>
                <div className="form-check">
                    <Field
                        className="form-check-input"
                        name="type_payment"
                        component="input"
                        type="radio"
                        value="payment_per_hour"
                        id="payment_per_hour"
                    />
                    <label htmlFor="payment_per_hour">Оплата за час</label>
                </div>
                {
                    typePayment !== 'minimum_wage'
                        ?
                        <div className="px-4">
                            <div className="check-wrap">
                                <span>Указать с НДФЛ</span>
                                <div className="custom-control custom-switch">
                                    <Field
                                        className="custom-control-input"
                                        name="isPersonalIncomeTax"
                                        component="input"
                                        type="checkbox"
                                        value="personalIncomeTax"
                                        id="personalIncomeTax"
                                    />
                                    <label className="custom-control-label" htmlFor="personalIncomeTax">Без НДФЛ</label>
                                </div>
                            </div>
                            <div className="form-group input-wrap">
                                <Field
                                    className="form-control"
                                    name="amount"
                                    component="input"
                                    type="number"
                                    value="personalIncomeTax"
                                />
                                <p>
                                    <span>&#8381;&nbsp;
                                        {
                                            typePayment === 'payment_per_day'
                                                ? 'в день'
                                                : typePayment === 'payment_per_hour'
                                                ? 'в час'
                                                : ''
                                        }
                                    </span>
                                </p>
                            </div>
                        </div>
                        : <></>
                }
            </div>
        </form>
    )
}

Form = reduxForm({
    form: 'payment',
    initialValues: {
        type_payment: 'monthly_salary',
        isPersonalIncomeTax: false,
        amount: 40000
    },
})(Form)

const selector = formValueSelector('payment')
Form = connect(state => {
    const typePayment = selector(state, 'type_payment')
    return {
        typePayment
    }
})(Form)

export default Form